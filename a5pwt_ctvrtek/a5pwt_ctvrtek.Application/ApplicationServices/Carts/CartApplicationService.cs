﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using a5pwt_ctvrtek.Application.Mappers.Carts;
using a5pwt_ctvrtek.Application.ViewModels.Carts;
using a5pwt_ctvrtek.Domain.Entities.Carts;
using a5pwt_ctvrtek.Domain.Services.Carts;

namespace a5pwt_ctvrtek.Application.ApplicationServices.Carts
{
    public class CartApplicationService : ICartApplicationService
    {
        private readonly ICartService _cartService;
        private readonly ICartMapper _cartMapper;

        public CartApplicationService(
            ICartService cartService,
            ICartMapper cartMapper)
        {
            _cartService = cartService;
            _cartMapper = cartMapper;
        }

        public CartItem AddToCart(int productID, int amount, string userTrackingCode)
        {
            return _cartService.AddToCart(productID, amount, userTrackingCode);
        }

        public IndexViewModel GetIndexViewModel(string userTrackingCode)
        {
            var items = _cartService.GetCartItems(userTrackingCode);
            return new IndexViewModel
            {
                CartItems = _cartMapper.GetViewModelsFromEntities(items),
                Total = items.Sum(x => x.Product.Price * x.Amount)
            };
        }

        public void RemoveFromCart(int productID, string userTrackingCode)
        {
            _cartService.RemoveFromCart(productID, userTrackingCode);
        }
    }
}
